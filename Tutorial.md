
# MyBBS React Crash Course

## Getting started

1. Initialize new git project

2. Create React App
```bash
npx create-react-app my-react-app
cd my-react-app
npm start
```
Install required dependencies via **yarn** (the same as **npm** basically)
```bash
yarn add react-redux redux-logger
```

3. JSX
```jsx
import React from 'react'	// should always be imported

const props = {
	label: "label",
	title: "title",
}

function Func(props) {
	// do stuff
}

const AlsoFunc = (props) => {
	// do stuff
}

const AnotherFunc = ({ label, title }) => {
	// do stuff
}

const FunctionGenerator = (props) => (alsoProps) => {
	// do stuff
}

class ItemsView extends React.Component {
	render() {
		return (
			<div>
				<p>hello world!</p>
			</div>
		)
	}
}


// generic button component
export const Button = ({ label, onClick, isVisible = false, ...props }) => isVisible ? (
	<button type="button" onClick={onClick} {...props}>
		{label}
	</button>
) : null


// specific button
export const ConfirmButton = () => {
	const onClickConfirm = () => {
		// do stuff
	}
	
	return (
		<Button
			isVisible={true}
			label="Confirm"
			onClick={this.onClickConfirm}
			className="button-confirm"
		/>
}
```

4. State and Props
```jsx
import React from 'react'
import { ButtonLoading, JustButton } from '../common/components'

class CustomButton extends React.Component {
	render() {
		const { isFetching } = this.props

		if (isFetching) {
			return <ButtonLoading />
		}

		return <JustButton />
	}
}

class ItemsView extends React.Component {
	state = {
		isFetching: false,
	}
	
	render() {
		const { isFetching } = this.state
		
		return (
			<div>
				<h1>Header</h1>
				<CustomButton isFetching={isFetching} />
			</div>
		)
	}
}
```

5. Lifecycle Methods

```jsx
import React from 'react'

class ItemsView extends React.Component {
	// called once when the component is created
	componentDidMount() {
		console.log("I AM MOUNTED")
	}

	// called on every component rerender
	componentDidUpdate() {
		console.log("PROBABLY MY PROPS HAVE CHANGED")
	}
	
	render() {		
		return (
			<div>
				<h1>Header</h1>
			</div>
		)
	}
}
```

6. Redux state machine

Typical file structure for a module using Redux
```bash
src
  common
  comments
  posts
    components
	  PostsView.js
      PostComponent.js
    selectors.js
    actions.js
    actionTypes.js
    reducer.js
  store.js
  index.js
```

Action types, actions, selectors and reducer
```js
// action types
export const FETCH_POSTS = "FETCH_POSTS"
export const FETCH_POSTS_SUCCESS = "FETCH_POSTS_SUCCESS"
export const FETCH_POSTS_ERROR = "FETCH_POSTS_ERROR"
```
```js
// actions
export const fetchPosts = () => ({ type: FETCH_POSTS })
export const fetchPostsSuccess = payload => ({ type: FETCH_POSTS_SUCCESS, payload })
export const fetchPostsError = payload => ({ type: FETCH_POSTS_ERROR, payload })
```
```js
export  const  initialState = {
	posts: [],
	isFetching:  false,
}

export  const  reducer = (state = initialState, action) => {
	switch (action.type) {
		case  FETCH_POSTS:
			return {
				...state,
				isFetching:  true,
			}

		case  FETCH_POSTS_ERROR:
			return {
				...state,
				isFetching:  false,
			}

		case  FETCH_POSTS_SUCCESS:
			return {
				...state,
				isFetching:  false,
				posts:  action.payload,		// saving response data here
			}

		default:
			return  state
	}
}
```
```js
// selectors
export const getPosts = state => state.posts.posts
export const getIsFetching = state => state.posts.isFetching
```

Store creator (store.js)
```js
import { applyMiddleware, combineReducers, createStore } from  'redux'
import  logger  from  'redux-logger'
import { initialState  as  commentsInitialState, reducer  as  commentsReducer } from  './comments/reducer'
import { initialState  as  postsInitialState, reducer  as  postsReducer } from  './posts/reducer'

const  rootReducer = combineReducers({
	posts:  postsReducer,
	comments:  commentsReducer,
})

const  preloadedState = {
	posts:  postsInitialState,
	comments:  commentsInitialState,
}

export  const  configureStore = () =>  createStore(
	rootReducer,
	preloadedState,
	applyMiddleware(logger),
)
```

index.js
```js
import React  from 'react'
import ReactDOM  from 'react-dom'
import { Provider } from 'react-redux'
import './index.css'
import { configureStore } from './store'
import { PostsView } from './posts/components/PostsView'

const store = configureStore()

ReactDOM.render(
	<Provider  store={store}>
		<PostsView  />
	</Provider>,
	document.getElementById('root')
);
```

PostsView.js
```jsx
import  React  from  'react'
import { connect } from  'react-redux'
import  *  as  actions  from  '../actions'
import { getIsFetching, getPosts } from  '../selectors'
import { PostItem } from  './PostItem'

class  _PostsView  extends  React.Component {
	componentDidMount() {
		const {
			fetchPosts,
			fetchPostsSuccess,
			fetchPostsError,
		} = this.props

		fetchPosts()

		fetch('https://jsonplaceholder.typicode.com/posts')
			.then((response) =>  response.json())
			.then((data) => {
				fetchPostsSuccess(data)
			})
			.catch(err  => {
				fetchPostsError(err)
			})
	}

	render () {
		const { posts } = this.props

		if (posts.length === 0) {
		return  null
		}

		return (
		<div>
		<h1>Posts:</h1>
			{posts.map(post  =>  <PostItem  item={post}  key={`${post.userId}${post.id}`}  />)}
		</div>
		)
	}
}

const  mapStateToProps = state  => ({
	posts:  getPosts(state),
	isFetching:  getIsFetching(state),
})

export  const  PostsView = connect(mapStateToProps, actions)(_PostsView)
```
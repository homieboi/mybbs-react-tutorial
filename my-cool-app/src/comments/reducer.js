import {
    FETCH_COMMENTS,
    FETCH_COMMENTS_SUCCESS,
    FETCH_COMMENTS_ERROR,
} from './actionTypes'

export const initialState = {
    comments: {},
}

export const reducer = (state = initialState, action) => {
    switch (action.type) {

        case FETCH_COMMENTS: {
            const { postId } = action.payload

            return {
                ...state,
                comments: {
                    ...state.comments,
                    [postId]: {
                        isFetching: true,
                        data: [],
                    },
                },
            }
        }

        case FETCH_COMMENTS_SUCCESS: {
            const { postId, data } = action.payload

            return {
                ...state,
                comments: {
                    ...state.comments,
                    [postId]: {
                        isFetching: false,
                        data,
                    },
                },
            }
        }

        case FETCH_COMMENTS_ERROR:
            return state

        default:
            return state
    }
}
export const getComments = (state, postId) => state.comments.comments[postId] && state.comments.comments[postId].data

export const getIsFetching = (state, postId) => state.comments.comments[postId] && state.comments.comments[postId].isFetching
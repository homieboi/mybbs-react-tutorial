import { FETCH_COMMENTS, FETCH_COMMENTS_SUCCESS, FETCH_COMMENTS_ERROR } from "./actionTypes"

export const fetchComments = ({ postId }) => ({ type: FETCH_COMMENTS, payload: { postId } })

export const fetchCommentsSuccess = ({ postId, data }) => ({ type: FETCH_COMMENTS_SUCCESS, payload: { postId, data } })

export const fetchCommentsError = ({ postId, err }) => ({ type: FETCH_COMMENTS_ERROR, payload: { postId, err } })
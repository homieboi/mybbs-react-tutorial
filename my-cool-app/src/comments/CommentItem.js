import React from 'react'

export const CommentItem = ({
    item,
}) => (
    <div className="comment">
        <h4>{item.email}</h4>
        <p>{item.name}</p>
        <p>{item.body}</p>
    </div>
)
import React from 'react'
import { Button } from './Button'

export const LoadPostsButton = ({ isFetching, onClick }) => (
    <Button className="load-posts-button" onClick={onClick}>
        {isFetching ? <p>Loading...</p> : <p>Load Posts</p>}
    </Button>
)
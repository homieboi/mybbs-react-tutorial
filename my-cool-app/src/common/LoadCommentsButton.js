import React from 'react'
import { Button } from './Button'

export const LoadCommentsButton = ({ isFetching, onClick }) => (
    <Button className="load-comments-button" onClick={onClick}>
        {isFetching ? <p>Loading</p> : <p>Load Comments</p>}
    </Button>
)
import React from 'react'
import { connect } from 'react-redux'
import { CommentItem } from '../../comments/CommentItem'
import { getComments, getIsFetching } from '../../comments/selectors'
import { LoadCommentsButton } from '../../common/LoadCommentsButton'

const mapStateToProps = (state, props) => ({
    isFetchingComments: getIsFetching(state, props.item.id),
    comments: getComments(state, props.item.id),
})

const withConnect = connect(mapStateToProps)

export const PostItem = withConnect(
    ({ item, onFetchComments, isFetchingComments, comments }) => {
        const { userId, title, body } = item

        return (
            <div className="post">
                <h4>{`${title} (user #${userId})`}</h4>
                <p>{body}</p>
                {comments && comments.length ? (
                    <div>
                        {comments.map(comment => <CommentItem item={comment} key={comment.name} />)}
                    </div>
                ) : (
                    <LoadCommentsButton onClick={onFetchComments} isFetching={isFetchingComments} />
                )}
            </div>
        )
    }
)
import React from 'react'
import { connect } from 'react-redux'
import * as commentsActions from '../../comments/actions'
import { LoadPostsButton } from '../../common/LoadPostsButton'
import * as postsActions from '../actions'
import { getIsFetching, getPosts } from '../selectors'
import { PostItem } from './PostItem'

class _PostsView extends React.Component {
    componentDidMount() {
        // this._fetchPosts()
    }

    _fetchCommentsFunctionCreator = postId => () => {
        const {
            fetchComments,
            fetchCommentsSuccess,
            fetchCommentsError,
        } = this.props

        fetchComments({ postId })

        setTimeout(() => {
            fetch(`https://jsonplaceholder.typicode.com/posts/${postId}/comments`)
                .then((response) => response.json())
                .then((data) => fetchCommentsSuccess({ postId, data }))
                .catch((err) => fetchCommentsError({ postId, err }))
        }, 2000)
    }
        
    _fetchPosts = () => {
        const {
            fetchPosts,
            fetchPostsSuccess,
            fetchPostsError,
        } = this.props

        fetchPosts()

        setTimeout(() => {
            fetch('https://jsonplaceholder.typicode.com/posts')
                .then((response) => response.json())
                .then((data) => fetchPostsSuccess(data))
                .catch(err => fetchPostsError(err))
        }, 2000)
    }

    render () {
        const { posts, isFetchingPosts } = this.props

        if (posts.length === 0) {
            return (
                <div>
                    <LoadPostsButton isFetching={isFetchingPosts} onClick={this._fetchPosts} />
                </div>
            )
        }

        return (
            <div>
                <h1>Posts:</h1>
                {posts.map(post => (
                    <PostItem
                        item={post}
                        key={`${post.userId}${post.id}`}
                        onFetchComments={this._fetchCommentsFunctionCreator(post.id)}
                    />
                ))}
            </div>
        )
    }
}

const mapStateToProps = state => ({
    posts: getPosts(state),
    isFetchingPosts: getIsFetching(state),
})

export const PostsView = connect(mapStateToProps, { ...postsActions, ...commentsActions })(_PostsView)
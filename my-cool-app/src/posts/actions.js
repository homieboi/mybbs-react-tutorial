import { FETCH_POSTS, FETCH_POSTS_SUCCESS, FETCH_POSTS_ERROR } from './actionTypes'

export const fetchPosts = () => ({ type: FETCH_POSTS })

export const fetchPostsSuccess = payload => ({ type: FETCH_POSTS_SUCCESS, payload })

export const fetchPostsError = payload => ({ type: FETCH_POSTS_ERROR, payload })
import {
    FETCH_POSTS,
    FETCH_POSTS_SUCCESS,
    FETCH_POSTS_ERROR,
} from './actionTypes'

export const initialState = {
    posts: [],
    isFetching: false,
}

export const reducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_POSTS:
            return {
                ...state,
                isFetching: true,
            }

        case FETCH_POSTS_ERROR:
            return {
                ...state,
                isFetching: false,
            }

        case FETCH_POSTS_SUCCESS:
            return {
                ...state,
                isFetching: false,
                posts: action.payload,
            }
        
        default:
            return state
    }
}
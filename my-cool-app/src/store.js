import { applyMiddleware, combineReducers, createStore } from 'redux'
import logger from 'redux-logger'
import { initialState as commentsInitialState, reducer as commentsReducer } from './comments/reducer'
import { initialState as postsInitialState, reducer as postsReducer } from './posts/reducer'

const rootReducer = combineReducers({
    posts: postsReducer,
    comments: commentsReducer,
})

const preloadedState = {
    posts: postsInitialState,
    comments: commentsInitialState,
}

export const configureStore = () => createStore(
    rootReducer,
    preloadedState,
    applyMiddleware(logger),
)
import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import './index.css'
import { PostsView } from './posts/components/PostsView'
import { configureStore } from './store'


const store = configureStore()

ReactDOM.render(
  <Provider store={store}>
    <PostsView />
  </Provider>,
  document.getElementById('root')
);